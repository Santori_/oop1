package Week4.Cat;

public class CatOwner {
    Cat cat;

    public CatOwner(Cat cat){
        this.cat = cat;
    }
    public void WalkWithCat() throws CatIsNotReadyException {
        cat.walk();
    }

}
