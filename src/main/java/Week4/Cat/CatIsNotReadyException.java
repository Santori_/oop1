package Week4.Cat;

public class CatIsNotReadyException extends Exception{
    public CatIsNotReadyException(String message){
        super(message);
    }
}
