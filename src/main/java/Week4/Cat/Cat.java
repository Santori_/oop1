package Week4.Cat;

public class Cat {
    String name;
    boolean isLeashPutOn;
    boolean ate;

    public Cat(String name) {
        this.name = name;
    }

    public void putate(){
        System.out.println("Поел!");
        this.ate = true;
    }


    public void putLeash() {

        System.out.println("Поводок надет!");
        this.isLeashPutOn = true;
    }

    public void walk() throws CatIsNotReadyException {

        System.out.println("Собираемся на прогулку!");
        if (isLeashPutOn && ate) {
            System.out.println("Ура, идем гулять! " + name + " очень рад!");
        } else {
            throw new CatIsNotReadyException("Кот " + name + " не готов к прогулке! Проверьте экипировку!");
        }
    }
}
