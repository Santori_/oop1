package Week3.WeatherStation;

public class Main {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecasDisplay forecasDisplay = new ForecasDisplay(weatherData);

        weatherData.measurementsChanged(35, 40,50);

    }
}
